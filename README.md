# Symfony 5 docker containers

A Proof-of-concept of a running Symfony 5 application inside containers

```
git clone git@gitlab.com:martinpham/symfony-5-docker.git

cd symfony-5-docker

cd docker

docker-compose up
```

## Compose

### Database (MariaDB)

...

### PHP (PHP-FPM)

Composer is included

```
docker-compose run php-fpm composer 
```

To run fixtures

```
docker-compose run php-fpm bin/console doctrine:fixtures:load
```

### Webserver (Nginx)

...

### Debugging in Intelij
Create PHP Remote Debugging Session with the IDE Key `PHPSTORM` and the server configuration as following : 
IP : 10.254.254.254
Port: 9001

Use Path Mapping: src directory should be mapped to absolute path: `/var/www`